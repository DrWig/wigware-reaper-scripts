# Reaper python Script example for ET250-3D turntable
# comms code based on https://github.com/holoplot/ET250-python
import math
import socket
import argparse
import struct

#Global vars
timejump = 20; #time in seconds to skip at
anglejump = -2.5; #angle to increment in (+ve is clockwise!)
num_rotations = 1; #how many full rotations
ex_time = 10; #extra seconds at the very end, just in case
tottime = (timejump*num_rotations*360/abs(anglejump))+10; #how long (in seconds) do you want this to run for.  Project is stopped after this time automatically (so you can leave it unattended if you want/need.  Example is timejump * number of measurements * number of runs to do!
send_only = False; #send only true/false.  However, need to send to zero table!
#addr = "127.0.0.1"
addr = "192.168.1.34" #ip address of turntable

count = 0;    #count number of commands sent (sanity check)
playpos = 0.0;#current play cursor position
playstate = 0;#play state &1=playing,&2=pause,&=4 is recording
curpos = 0.0; #cursor position (if not playing)

tablestate = 10; # current table position
prevtablestate = 0; # previous table position
wig = 0             # debug variable
delay = 0;
tableconnected = False;

UDP_PORT = 6668     # table receive port
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.settimeout(0.25)

if not send_only:
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(("0.0.0.0", UDP_PORT))
    
COMMAND_MOVE_FORWARD = 1
COMMAND_MOVE_BACKWARD = 2
COMMAND_STOP = 3
COMMAND_READ_ANGLE = 4
COMMAND_SET_ZERO = 5

REPLY_OK = 0x33
REPLY_ERR = 0x66

STATUS_MOVING = 4
STATUS_STOPPED = 5

comm = "none"

# Funciton that will be looped looking at the play/cursor position
# and deciding what angle the table should currently be at
def WigCallback():
  "This is some helper text or something in python"
  global count;
  global playpos,curpos,playstate,tablestate,prevtablestate,comm;
  global tableconnected,delay,send_only,tottime;
  ret = 0

  if tableconnected==False:
    #check for tableconnected
    RPR_ClearConsole();
    RPR_ShowConsoleMsg("Looking for Turntable at : "+addr)
    if (delay % 100)==0:
      check = read_angle();
      if check!=450:
        tableconnected = True;
        prevtablestate = read_angle();
    delay = delay + 1;
  else: #if table is found:
    RPR_ClearConsole();
    RPR_ShowConsoleMsg("Cursor Position "+str(round(curpos,1)) + " seconds\n");
    RPR_ShowConsoleMsg("Play Position "+str(round(playpos,1))+" seconds\n");
    RPR_ShowConsoleMsg("Table Position "+str(tablestate)+" degrees\n\n");
    RPR_ShowConsoleMsg("Turntable IP : "+addr+"\n")
    
    #if count < 100:
    playpos = RPR_GetPlayPosition();  
    if playpos < 0.0: # fix bug where play pos is negative
      playpos = 0.0;
    playstate = RPR_GetPlayState();
    curpos = RPR_GetCursorPosition();
    
    #if (curpos==0.0 and playstate==0 and tablestate != 0):
      # reset turntable
      # do reset command here
      #tablestate = prevtablestate = 0;
    if playstate==0:
      tablestate = math.floor(curpos / timejump) * anglejump;
    else:  
      tablestate = math.floor(playpos / timejump) * anglejump;
    
    if tablestate!=prevtablestate:
      # move table here
      if tablestate==0:
        count = count+1;
        ret = move_zero()
        comm = "zero table"
      else:
        rotate = tablestate-prevtablestate;
        if rotate > 180:
          rotate = rotate - 360;
        elif rotate < -180:
          rotate = rotate + 360;
          
        if rotate>0:
          count = count+1;
          ret = move_forward(rotate)
          comm = "move_forward " + str(rotate)
        else:
          count = count+1;
          ret = move_backward(-rotate)
          comm = "move_backward " + str(-rotate)
          
      prevtablestate = tablestate;
  
    RPR_ShowConsoleMsg("Last Command : " + comm + "\n")
    RPR_ShowConsoleMsg("Last Response : "+ str(ret) + "\n")
    RPR_ShowConsoleMsg("Number of Commands Sent : " + str(count) + "\n")
    RPR_ShowConsoleMsg("Jump angle : " + str(anglejump) + " degrees\n")
    RPR_ShowConsoleMsg("Jump every " + str(timejump) + " seconds\n")
    RPR_ShowConsoleMsg("Project stops after " +str(tottime) + " seconds\n")

  if playpos>tottime:
    RPR_OnStopButton();  

  RPR_defer("WigCallback();");    
  return;
  
def send_command(command, arg = 0):
    global addr,wig
    checksum = ((command & 0xff) ^ ((arg >> 8) & 0xff)) ^ (arg & 0xff)
    message = struct.pack(">BhB", command, arg, checksum)
    try:
      sock.sendto(message, (addr, UDP_PORT))
    except socket.error:
      return [REPLY_ERR]

    if send_only:
      return [REPLY_OK]
    else:
      try:
        reply = sock.recvfrom(7)
      except socket.error:
        return [REPLY_ERR];
        
      return reply[0]
      #if (sys.getsizeof(reply))==7:
      #  wig = 7;
      
      #else:
      #  wig = 1;
      #  return [REPLY_ERR]

def check_simple_command(reply):
    return reply[0] == REPLY_OK

def move_forward(degree):
    if degree == 0.0:
        return True

    reply = send_command(COMMAND_MOVE_FORWARD, int(degree * 10))
    return check_simple_command(reply)

def move_backward(degree):
    if degree == 0.0:
        return True

    reply = send_command(COMMAND_MOVE_BACKWARD, int(degree * 10))
    return check_simple_command(reply)

def set_zero():
    if send_only:
        print("ERROR: The ZERO command does not work in send-only mode")
        return [REPLY_ERR]

    reply = send_command(COMMAND_SET_ZERO)
    return check_simple_command(reply)

def read_angle():
    global wig,send_only;
    reply = send_command(COMMAND_READ_ANGLE)
    wig = sys.getsizeof(reply)
    if not send_only and sys.getsizeof(reply)==44:
      (status, degree, direction, checksum) = struct.unpack(">BIBB", reply)
    #wig = 10;
    else:
      degree = 4500;
        
  #print("status=%d degree=%d dir=%d" % (status, degree, direction))
    wig = sys.getsizeof(reply[0])
    #wig = struct.calcsize(">BIBB");
    return float(degree / 10.0)

def move_zero():
    degree = read_angle()
    if degree==0:
      return True
    elif (degree > 180):
        move_forward(360 - degree)
    else:
        move_backward(degree)

    return True

def stop():
    reply = send_command(COMMAND_STOP)
    return check_simple_command(reply)
    
####ACTUAL SCRIPT STARTS HERE!!!!!#####
WigCallback();
####END OF ACTUAL SCRIPT!!!!###########

